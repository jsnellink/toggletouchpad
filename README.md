### Toggle Touchpad 

##### Why?
This script was created very much as a hobby project to learn a bit more about the Windows powershell.
For me as a Macbook Pro (2017) - Windows 10 user it solves a very particular problem.
The touchpad of the Macbook Pro is very big, and the palm-rejection on Bootcamp Windows 10 is not particularly good. This means that when coding, half the time I accidentally select things. This got so annoyed that I bought an external wireless mouse (if you're curious which one, check out the script). But then I got annoyed I had to go to device manager every time to disable the touchpad. And to leave it disabled is to play with fire. This script takes care of this problem by automating that entire process behind a simple shortcut that can be run from your task bar or desktop.

##### Installation
You simply copy over the code or clone the repository. Then you'll have to do 2 things:
1. Assign a FriendlyName to both your external mouse and internal trackpad devices.
Basically for each device you go to device manager -> properties -> details -> dropdown -> driver key
You use this unique driver key to find the device in the Windows Registry. When you found the device 
in the registry you simply create a new string named "FriendlyName" at that registry location. Follow this tutorial if you need more help:
https://www.eightforums.com/threads/tutorial-how-to-change-device-names-in-device-manager.15321/
2. Assign these FriendlyNames to the $externalMouse & $internalTrackpad variables of the ToggleTouchpad.ps1 script.
3. Make sure to update the filepath in the shortcut if you plan to run the code from there.

##### What does it do exactly?
So the script starts through 'RunElevatedToggleTrackpad.ps1', an elevated powershell that checks if your trackpad is enabled. If so it then also checks if your external mouse is connected, and disables your trackpad. If the trackpad is disabled it will simply immediately enable the trackpad.

##### How?
The script works by narrowing down a search by looking for all devices of class "Mouse" and then further checks for all mice with a particular "FriendlyName". 

##### Future
I plan to actually upgrade this script to some kind of background service that registers if my external mouse is connected and will automatically disable/enable the trackpad. For now that seemed too much bother though as even in an emergency it is possible to navigate to the shortcut in the taskbar no problem.