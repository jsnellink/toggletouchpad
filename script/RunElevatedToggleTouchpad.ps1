# Credits to: https://ss64.com/ps/syntax-elevate.html

If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
{
  # Relaunch as an elevated process:
  Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
  exit
}
# Now running elevated so launch the script:
set-executionpolicy remotesigned -Confirm:$false

$scriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path
& "$scriptDir\ToggleTouchpad.ps1"
