# Created by Joost Snellink - 2019
#Requires -RunAsAdministrator

# Note:
# For this script to work you have to set the 'FriendlyName' of 
# both your external mouse as your trackpad to something that you
# set the below variables to. This can be done through the Windows
# registry by searching for the device's "Driver key". This driver
# key can be found in the properties -> detail -> dropdown of the
# devices.

#FriendlyName of External Mouse
$externalMouse = 'Logitech G603 Lightspeed'
#FriendlyName of Touchpad
$internalTouchpad = 'Macbook Pro Touchpad'

# Check the current status of the Macbook Pro Touchpad
# If 'Macbook Pro Touchpad' status == OK (enabled)
if(Get-PnpDevice | Where-Object {$_.Class -match 'Mouse'} | Where-Object {$_.FriendlyName -match $internalTouchpad} | Where-Object {$_.Status -match 'OK'})
{
	# Then check first if the Logitech G603 is connected
	if(Get-PnpDevice | Where-Object { $_.Class -match 'Mouse' } | Where-Object { $_.FriendlyName -match $externalMouse } | Where-Object { $_.Status -match 'OK' })
		# If this is the case then find and DISABLE the Macbook Pro Touchpad
		{
			Get-PnpDevice | Where-Object { $_.Class -match 'Mouse' } | Where-Object { $_.FriendlyName -match $internalTouchpad } | 
			Disable-PnpDevice -Confirm:$false
			exit
		}
}

# Else enable the Touchpad
Get-PnpDevice | Where-Object {$_.Class -match 'Mouse'} | Where-Object {$_.FriendlyName -match $internalTouchpad} | Where-Object {$_.Status -match 'ERROR'} |
Enable-PnpDevice -Confirm:$false
exit